#!/bin/bash

set -ex

export INTELDIR=/opt/intel/19.1/
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib/intel64
export FC=ifort
export CC=icc
export CXX=icpc

mkdir tmp
cd tmp

wget https://www.mpich.org/static/downloads/3.3.2/mpich-3.3.2.tar.gz
tar xaf mpich-3.3.2.tar.gz
cd mpich-3.3.2
./configure --prefix=$HOME/ext
make -j8
make install
cd ../..

rm -rf tmp
