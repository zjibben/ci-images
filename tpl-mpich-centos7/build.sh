#!/bin/bash

set -ex

# Install CMake
wget -qO- https://github.com/Kitware/CMake/releases/download/v3.16.3/cmake-3.16.3-Linux-x86_64.tar.gz | sudo tar xz --strip=1 -C /usr/local/
which cmake
cmake --version

# Install Python, numpy, h5py
mkdir scratch
cd scratch
git clone --depth=1 https://github.com/python-cmake-buildsystem/python-cmake-buildsystem.git
mkdir python-build
cd python-build
cmake -DCMAKE_INSTALL_PREFIX:PATH=${HOME}/ext/python-install ../python-cmake-buildsystem
make -j8
make install
cd ..
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
${HOME}/ext/python-install/bin/python get-pip.py
${HOME}/ext/python-install/bin/python -m pip install numpy h5py
cd ..
rm -rf scratch


# Load g++ version 9
source /opt/rh/devtoolset-9/enable
g++ --version


export INTELDIR=/opt/intel/19.1/
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib/intel64
export FC=ifort
export CC=icc
export CXX=icpc

# Prepend our `python` executable into path
export PATH="${HOME}/ext/python-install/bin:$PATH"

# Portage expects to find boost in the normal place
sudo ln -s /usr/include/boost169/boost /usr/include/boost

mkdir tmp
cd tmp
git clone https://gitlab.com/truchas/truchas-tpl.git
cd truchas-tpl
git checkout v15
mkdir build
cd build
cmake \
    -C ../config/linux-intel.cmake \
    -DCMAKE_INSTALL_PREFIX=${HOME}/ext/ \
    -DSEARCH_FOR_HDF5=no \
    -DSEARCH_FOR_NETCDF=no \
    -DBUILD_PORTAGE=ON \
    ..
make VERBOSE=1 -j8
cd ../../..
rm -rf tmp
