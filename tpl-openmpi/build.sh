#!/bin/bash

set -ex

export INTELDIR=/opt/intel/19.1/
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib/intel64
export FC=ifort
export CC=icc
export CXX=icpc


# Install CMake
wget -qO- https://github.com/Kitware/CMake/releases/download/v3.16.3/cmake-3.16.3-Linux-x86_64.tar.gz | sudo tar xz --strip=1 -C /usr/local/
which cmake
cmake --version


mkdir tmp
cd tmp

git clone https://gitlab.com/truchas/truchas-tpl.git
cd truchas-tpl
git checkout v15
mkdir build
cd build
cmake \
    -C ../config/linux-intel.cmake \
    -DCMAKE_INSTALL_PREFIX=$HOME/ext/ \
    -DSEARCH_FOR_HDF5=no \
    -DSEARCH_FOR_NETCDF=no \
    -DBUILD_PORTAGE=ON \
    ..
make
cd ../../..
rm -rf tmp
